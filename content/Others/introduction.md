+++
title = "ox-hugo でブログを書く"
author = ["iwaizm"]
date = 2020-02-01T00:00:00+09:00
categories = ["others"]
draft = false
+++

はじめまして，@alicecat03 と申します。ブログをはじめました。

<!--more-->

ブログの構成は以下のような感じです。

-   org-mode で記事を書く。
-   [ox-hugo](https://ox-hugo.scripter.co/)で org-mode の記事を[Hugo](https://gohugo.io)互換の markdown に変換する。
-   Gitlab に Hugo のウェブサイトを Push し，Gitlab Pages でホスティングする。

テーマは自作中のものです。変な挙動をするかもしれません。
