+++
title = "About"
author = ["iwaizm"]
draft = false
+++

## This Website {#this-website}

このサイトは GitLab Pages に Hugo を使って生成した HTML をホスティングしています。

<!--more-->

元々は Emacs の org-mode で書かれたテキストを ox-hugo によって Hugo 向けに変換しています。プログラミングやその他の趣味についてのメモを書いていきます。


## Me {#me}

インターネット上では主に alicecat03 という名前を使っています。
Spacemacs と GNU/Linux が好きな，週末プログラマです。


### Links {#links}

-   Blog :  <https://alicecat03.gitlab.io/>
-   Twitter : <https://twitter.com/alicecat03>
-   Gitlab : <https://gitlab.com/alicecat03>
-   GitHub : <https://github.com/alicecat03>
