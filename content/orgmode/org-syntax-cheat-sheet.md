+++
title = "Org Syntax チートシート"
author = ["iwaizm"]
categories = ["orgmode"]
draft = true
+++

![](./org-mode-unicorn-logo.png)
個人的に覚えておきたい Org Syntax ののメモ。公式の情報は[こちら](https://orgmode.org/worg/dev/org-syntax.html)から。

<!--more-->


## Syntax {#syntax}

```org
# コメント
* 見出し 1
** 見出し 2
*** 見出し 3
**** 見出し 4
***** 見出し 5
****** 見出し 6
水平線
 -----
1. アイテム
2. *太字*
  - /斜体/
  - _下線_
    - [X] +取消線+
    - [ ] =等幅(Monospaced)=

- [[https://example.com]]
- [[https://example.com][リンク]]
PRE1 RADIO POST1          ("radio" link)
<PROTOCOL:PATH>           ("angle" link)
PRE2 PROTOCOL:PATH2 POST2 ("plain" link)
[[PATH3]DESCRIPTION]      ("regular" link)

  |---|---|---|
  | A | B | C |
  |---|---|---|
  | a | 1 | 4 |
  | b | 2 | 5 |
  | c | 3 | 6 |
  |---|---|---|
```
